package com.softtek.academia.JdbcExample;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * Hello world!
 *
 */
public class App 
{
    public static void main( String[] args )
    {
    	System.out.println("MySQL JDBC Connection");

        List<Object> result = new ArrayList<>();

        String SQL_SELECT = "Select * from COLOR";
        
    	// auto close connection
        try (Connection conn = DriverManager.getConnection(
                "jdbc:mysql://127.0.0.1:3306/sesion3?serverTimezone=UTC#", "root", "1234")) {

            if (conn != null) {
                System.out.println("Connected to the database!");
                
                PreparedStatement preparedStatement = conn.prepareStatement(SQL_SELECT);

                ResultSet resultSet = preparedStatement.executeQuery();

                while (resultSet.next()) {
                    long id = resultSet.getLong("COLORID");
                    String name = resultSet.getString("NAME");
                    String hexValue = resultSet.getString("HEXVALUE");
                    System.out.println("ID: " + id + " color's name: " + name + " hex's value: " + hexValue);
                }
            } else {
                System.out.println("Failed to make connection!");
            }

        } catch (SQLException e) {
            System.err.format("SQL State: %s\n%s", e.getSQLState(), e.getMessage());
        } catch (Exception e) {
            e.printStackTrace();
        }   
    }
}
